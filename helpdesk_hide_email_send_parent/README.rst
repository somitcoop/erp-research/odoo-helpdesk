================================
Helpdesk Hide Email Send Parent
================================


This module will hide the email send and reply features implemented by `Helpdesk Automatic Stage Changes` inside Helpdesk tickets.

**Table of contents**

.. contents::
   :local:

Configuration
=============

This module is intended to be installable `Optionally` if you simultaneously have installed `Helpdesk Automatic Stage Changes` and `Mass Parent Ticket Generation` at the same time.
Otherwise installing it is not going to be useful.

Usage
=====

Once installed, you are ready to go!
You can check any Ticket where it should appear the yellow `Send Email` button inside the button box and now it should be hidden.


Known issues / Roadmap
======================

There are no issues for the moment.

Bug Tracker
===========

Bugs are tracked on `GitLab Issues <https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/issues>`_.
In case of trouble, please check there if your issue has already been reported.
If you spotted it first, help us smashing it by providing a detailed and welcomed feedback.

Do not contact contributors directly about support or help with technical issues.

Credits
=======

Authors
~~~~~~~

* SomIT SCCL
* Som Connexio SCCL


Contributors
~~~~~~~~~~~~

* `SomIT SCCL <https://somit.coop>`_:

    * José Robles <jose.robles@somit.coop>
    * Juan Manuel Regalado <juanmanuel.regalado@somit.coop>


* `Som Connexio SCCL <https://somconnexio.coop>`_:

    * Gerard Funosas <gerard.funosas@somconnexio.coop>


Maintainers
~~~~~~~~~~~

This module is maintained by the OCA.

.. image:: https://odoo-community.org/logo.png
   :alt: Odoo Community Association
   :target: https://odoo-community.org

OCA, or the Odoo Community Association, is a nonprofit organization whose
mission is to support the collaborative development of Odoo features and
promote its widespread use.

You are welcome to contribute. To learn how please visit https://odoo-community.org/page/Contribute.