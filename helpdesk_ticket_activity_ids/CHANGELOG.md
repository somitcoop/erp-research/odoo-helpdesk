# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.1.0.2] - 2024-10-23
### Fixed
- [#85](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/85) Drop `default_team_id` from context when creating mail activities from tickets. 

## [12.0.1.0.1] - 2024-10-18
### Fixed
- [#84](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/84) Update xml views inheritance with module dependency changes: helpdesk_ticket_mail_message

## [12.0.1.0.0] - 2024-02-19
### Added
- [#66](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/66) Add helpdesk_ticket_activity_ids module
