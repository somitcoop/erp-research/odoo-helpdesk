###################
 Helpdesk Automove
###################

We need to be able to define a definitive automatic moving of a ticket.

**Table of contents**

.. contents::
   :local:

***************
 Configuration
***************

No configuration needed for this module.

*******
 Usage
*******

To use this module, you need to go to Helpdesk -> Configuration ->
Stages and activate the "Auto Move" checkbox in the stage you want its
tickets to be automatically moved to the next stage.

************************
 Known issues / Roadmap
************************

There are no issues for the moment.

*************
 Bug Tracker
*************

Bugs are tracked on `GitLab Issues
<https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/issues>`_. In
case of trouble, please check there if your issue has already been
reported. If you spotted it first, help us smashing it by providing a
detailed and welcomed feedback.

Do not contact contributors directly about support or help with
technical issues.

*********
 Credits
*********

Authors
=======

-  SomIT SCCL
-  Som Connexio SCCL

Contributors
============

-  `SomIT SCCL <https://somit.coop>`_:

      -  Álvaro Mellado <alvaro.mellado@somit.coop>
      -  José Robles <jose.robles@somit.coop>

-  `Som Connexio SCCL <https://somconnexio.coop>`_:

      -  Gerard Funosas <gerard.funosas@somconnexio.coop>

Maintainers
===========

This module is maintained by the OCA.

.. image:: https://odoo-community.org/logo.png
   :alt: Odoo Community Association
   :target: https://odoo-community.org

OCA, or the Odoo Community Association, is a nonprofit organization
whose mission is to support the collaborative development of Odoo
features and promote its widespread use.

You are welcome to contribute. To learn how please visit
https://odoo-community.org/page/Contribute.
