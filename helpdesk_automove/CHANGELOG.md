# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.0.2.1] - 2024-03-20
### Fixed
- [#2](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/20) Fixed required field 'To Stage'
## [12.0.0.2.0] - 2024-02-19
### Added
- [#8](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/8) Add heldesk_automove module