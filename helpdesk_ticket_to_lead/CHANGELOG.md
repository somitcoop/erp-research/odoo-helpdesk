# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.1.2.0] - 2024-11-21
### Added
- [#79](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/79) Improved the conversion from html to plain text
## [12.0.1.1.0] - 2024-05-07
### Added
- [#29](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/29) Add helpdesk_ticket_to_lead module
