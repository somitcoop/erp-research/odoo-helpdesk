# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.1.0.0] - 2024-10-07
### Added
- [#81](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/81) Add helpdesk_email_reply_to_team