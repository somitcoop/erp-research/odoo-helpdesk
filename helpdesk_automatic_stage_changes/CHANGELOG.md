# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.1.0.0] - 2024-10-01
### Changed
- [#84](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/84) Refactor original module separating from it the helpdesk_ticket_mail_message module. 

## [12.0.0.2.6] - 2024-10-01
### Fixed
- [#66](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/66) Fix mail message's model "compute_date_subject" method. 

## [12.0.0.2.5] - 2024-10-01
### Added
- [#74](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/74) Add widget_list_message widget. 

## [12.0.0.2.4] - 2024-09-26
### Fixed
- [#77](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/77) Remove unused dependency and fix incorrect flagging of received emails.
