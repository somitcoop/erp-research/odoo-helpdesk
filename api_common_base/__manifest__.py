# Copyright 2024-SomItCoop SCCL(<https://gitlab.com/somitcoop>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "API Common Base",
    "version": "12.0.0.1.2",
    "depends": ["base", "base_rest", "auth_api_key"],
    "author": """
        Som It Cooperatiu SCCL,
        Som Connexió SCCL,
        Odoo Community Association (OCA)
    """,
    "website": "https://gitlab.com/somitcoop/erp-research/odoo-helpdesk",
    "license": "AGPL-3",
    "summary": """
        REST API Common Controller
    """,
    "data": [],
    "demo": ["demo/api_key.xml"],
    "application": False,
    "installable": True,
}
