# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.0.1.2] - 2024-06-013
### Added
- [#36](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/36) Allow params to http_get common service method for tests

## [12.0.0.1.1] - 2024-06-05
### Added
- [#32](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/32) Add api_key demo for dependency tests purposes

## [12.0.0.1.0] - 2024-06-05
### Added
- [#31](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/31) Add api_common_base module
