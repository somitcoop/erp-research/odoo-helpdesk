# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.1.0.2] - 2024-07-09
### Changed
- [#52](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/52) Fixed some bad translations

## [12.0.1.0.1.1] - 2024-05-07
### Changed
- [#28](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/28) Added an icon for the module

## [12.0.1.0.1.0] - 2024-03-19
### Changed
- [#25](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/25) Necessary changes to improve user experience and functionality.
  - Updated the button labels, icons, and contexts in the helpdesk_ticket_merge_view.xml file.
  - Sets merge_ticket_id as not required in the WizardMerge class of the helpdesk_split_and_merge module.

## [12.0.1.0.0.1] - 2024-03-13
### Added
- [#4](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/4) Add helpdesk_split_and_merge module
