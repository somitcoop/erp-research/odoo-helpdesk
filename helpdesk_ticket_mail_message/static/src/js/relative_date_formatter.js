odoo.define('helpdesk_automatic_stage_changes.RelativeDateFormatter', function (require) {
    'use strict';

    var ListRenderer = require('web.ListRenderer');
    var FieldDate = require('web.basic_fields').FieldDate;
    var field_registry = require('web.field_registry');
    var _t = require('web.core')._t; // Per a traduccions

    /**
     * Function to format dates to strings based on code from:
     * mail/static/src/js/activity.js setDelayLabel
     *
     * @param {moment} date - Date to format.
     * @return {string} - Formatted date.
     */
    function formatRelativeDate(date) {
        var today = moment().startOf('day');
        var diff = date.diff(today, 'days', true); // true means no rounding
        if (diff === 0) {
            return _t("Today");
        } else if (diff < 0) { // Overdue
            if (diff === -1) {
                return _t("Yesterday");
            } else {
                return _.str.sprintf(_t("%d days overdue"), Math.abs(Math.floor(diff)));
            }
        } else { // Due
            if (diff === 1) {
                return _t("Tomorrow");
            } else {
                return _.str.sprintf(_t("Due in %d days"), Math.floor(diff));
            }
        }
    }

    ListRenderer.include({
        init: function (parent, state, params) {
            this._super.apply(this, arguments);
            this.date_fields = '[]';
            if (typeof this.state.getContext().date_fields_from_now != 'undefined'){
                this.date_fields = this.state.getContext().date_fields_from_now;
            }

        },
        _renderBodyCell: function (record, node, colIndex, options) {
            var $td = this._super.apply(this, arguments);
            if (node.attrs.name.includes(this.date_fields)) {
                var date_value = $($td).text().trim();
                if (date_value) {
                    var moment_date = moment(date_value, moment.localeData().longDateFormat('L'));
                    var formatted_date = formatRelativeDate(moment_date);
                    $td.html($('<div>').text(formatted_date));
                }
            }
            return $td;
        },
    });

    var FieldDate = require('web.basic_fields').FieldDate;
    var field_registry = require('web.field_registry');

    var RelativeDateWidget = FieldDate.extend({
        _render: function () {
            if (this.value) {
                var moment_date = moment(this.value, moment.localeData().longDateFormat('L'));
                var formatted_date = formatRelativeDate(moment_date);
                this.$el.text(formatted_date);
            }
        },
    });

    field_registry.add('relative_date', RelativeDateWidget);

    return RelativeDateWidget, ListRenderer;
});