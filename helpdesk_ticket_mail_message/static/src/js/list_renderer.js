odoo.define('helpdesk_ticket_mail_message.ListRenderer', function (require) {
    "use strict";

    var ListRenderer = require('web.ListRenderer');
    var view_registry = require('web.view_registry');
    var FieldOne2Many = require('web.relational_fields').FieldOne2Many;
    var fieldRegistry = require('web.field_registry');

    var ListRendererMailIcon = ListRenderer.extend({
        _renderView: function () {
            var $div_filter_buttons = $('<div>');
            $div_filter_buttons.addClass('btn-group');
            $div_filter_buttons.css('padding', '10px 0px 0px 0px');
            // Renderizar el botón usando QWeb
            var $filterButton_email_sent = $('<button>');
            $filterButton_email_sent.addClass('btn btn-secondary filter-custom');
            var $i_email_sent = $('<i>');
            $i_email_sent.addClass('fa fa-long-arrow-left color-red');
            $filterButton_email_sent.append($i_email_sent);

            var $filterButton_email_received = $('<button>');
            $filterButton_email_received.addClass('btn btn-secondary filter-custom');
            var $i_email_received = $('<i>');
            $i_email_received.addClass('fa fa-long-arrow-right color-black');
            $filterButton_email_received.append($i_email_received);

            var $filterButton_note = $('<button>');
            $filterButton_note.addClass('btn btn-secondary filter-custom');
            var $i_note = $('<i>');
            $i_note.addClass('fa fa-file-text-o color-green');
            $filterButton_note.append($i_note);

            // Añadir el botón a la barra de botones
            $div_filter_buttons.append($filterButton_email_sent);
            $div_filter_buttons.append($filterButton_email_received);
            $div_filter_buttons.append($filterButton_note);


            // Asignar evento al botón para alternar el filtro sin recargar
            $filterButton_email_sent.on("click", this._onClickFilter.bind(this, $filterButton_email_sent));
            $filterButton_email_received.on("click", this._onClickFilter.bind(this, $filterButton_email_received));
            $filterButton_note.on("click", this._onClickFilter.bind(this, $filterButton_note));

            var $el = this._super.apply(this, arguments);
            this.$el.prepend($div_filter_buttons);
            return $el;
        },

        _rgb2hex: function (rgb) {
            // Asegurarse de que rgb es un string válido
            const match = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            if (!match) {
                throw new Error("Formato RGB inválido");
            }

            // Extraer los valores de R, G, B
            const r = parseInt(match[1], 10);
            const g = parseInt(match[2], 10);
            const b = parseInt(match[3], 10);

            // Convertir a hexadecimal y asegurar que siempre tengan 2 dígitos
            return (
                "#" +
                ("0" + r.toString(16)).slice(-2) +
                ("0" + g.toString(16)).slice(-2) +
                ("0" + b.toString(16)).slice(-2)
            );
        },

        _onClickFilter: function ($button) {
            var self = this;
            var $buttons = $('button.filter-custom');
            $buttons.each(function () {
                if ($button[0] != $(this)[0] && self._rgb2hex($(this).css('background-color')) == '#7c7bad') {
                    self._onToggleFilter($(this));
                }
            });
            self._onToggleFilter($button);
        },

        _onToggleFilter: function ($button) {
            // Cambiar el estado del filtro
            if ($button.find("i").hasClass("fa-long-arrow-left color-red")) {
                $("tr.o_data_row").each(function () {
                    // Busca dentro de cada <tr> si hay un <td> que contiene un <i> cuya clase NO sea .fa-long-arrow-left.color-red
                    const hasInvalidIcon = $(this).find("td:first-child i:not(.fa.fa-long-arrow-left.color-red)").length > 0;

                    // Si cumple la condición y no está oculto
                    if (hasInvalidIcon && $(this).is(":visible")) {
                        $(this).hide(); // Oculta el <tr>
                        console.log("Ocultando el <tr>:", $(this));
                        $button.css('background-color', '#7C7BAD;');
                    }
                    // Si no cumple la condición pero está oculto, lo mostramos
                    else if (hasInvalidIcon && $(this).is(":hidden")) {
                        $(this).show(); // Muestra el <tr>
                        console.log("Mostrando el <tr>:", $(this));
                        $button.removeAttr("style");
                    }
                });
            } else if ($button.find("i").hasClass("fa-long-arrow-right color-black")) {
                $("tr.o_data_row").each(function () {
                    const hasInvalidIcon = $(this).find("td:first-child i:not(.fa.fa-long-arrow-right.color-black)").length > 0;
                    if (hasInvalidIcon && $(this).is(":visible")) {
                        $(this).hide(); // Oculta el <tr>
                        console.log("Ocultando el <tr>:", $(this));
                        $button.css('background-color', '#7C7BAD;');
                    }
                    else if (hasInvalidIcon && $(this).is(":hidden")) {
                        $(this).show(); // Muestra el <tr>
                        console.log("Mostrando el <tr>:", $(this));
                        $button.removeAttr("style");
                    }
                });
            } else if ($button.find("i").hasClass("fa-file-text-o color-green")) {
                $("tr.o_data_row").each(function () {
                    const hasInvalidIcon = $(this).find("td:first-child i:not(.fa.fa-file-text-o.color-green)").length > 0;
                    if (hasInvalidIcon && $(this).is(":visible")) {
                        $(this).hide(); // Oculta el <tr>
                        console.log("Ocultando el <tr>:", $(this));
                        $button.css('background-color', '#7C7BAD;');
                    }
                    else if (hasInvalidIcon && $(this).is(":hidden")) {
                        $(this).show(); // Muestra el <tr>
                        console.log("Mostrando el <tr>:", $(this));
                        $button.removeAttr("style");
                    }
                });
            }
        },

        // We hide the value of a column in a list of a specific field.
        _renderHeaderCell: function (node) {
            var $th = this._super.apply(this, arguments);
            var name = node.attrs.name;
            if (name == "message_type_mail") {
                $th.html("");
            }
            return $th;
        },
        //We redesign how a particular field in the list will be displayed
        _renderBodyCell: function (record, node, colIndex, options) {
            var $td = this._super.apply(this, arguments);
            var name = node.attrs.name;
            if (name == "message_type_mail") {
                var value = record.data[name];
                var $i = $('<i>');

                if (value == "email_sent") {
                    $i.addClass('fa fa-long-arrow-left color-red');
                    $i.attr('title', 'To: ' + record.data['email_from']);
                } else if (value == "email_received") {
                    $i.addClass('fa fa-long-arrow-right color-black');
                    $i.attr('title', 'From: ' + record.data['email_from']);
                } else if (value == "note") {
                    $i.addClass('fa fa-file-text-o color-green');
                    $i.attr('title', 'User: ' + record.data['email_from']);
                }

                $td.html($i);

                $td.attr("style", "text-align: center;");
            } else if (name == "date_subject") {
                $td.attr('title',
                    moment.utc(record.data['date']).format("YYYY-MM-DD HH:MM:SS"));
            } else if ($td.find(".fa-reply").length == 1) {
                $i = $td.find(".fa-reply");
                $i.attr('title', $i.parent().attr("help"));
            } else if ($td.find(".fa-share").length == 1) {
                $i = $td.find(".fa-share");
                $i.attr('title', $i.parent().attr("help"));
            } else if ($td.find(".fa-reply-all").length == 1) {
                $i = $td.find(".fa-reply-all");
                $i.attr('title', $i.parent().attr("help"));
            }


            return $td;
        },
    });

    view_registry.add('mail_icon', ListRendererMailIcon);

    var ListRendererMailIconFieldOne2Many = FieldOne2Many.extend({
        _getRenderer: function () {
            if (this.view.arch.tag === 'tree') {
                return ListRendererMailIcon;
            }
            return this._super.apply(this, arguments);
        },
    });

    fieldRegistry.add('list_mail_icon_one2many', ListRendererMailIconFieldOne2Many);

    return ListRendererMailIcon, ListRendererMailIconFieldOne2Many;
});
