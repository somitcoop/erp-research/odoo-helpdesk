# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.1.1.1] - 2024-12-05
### Added
- [#89](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/89) Fix relative_date_formatter according to localeData

## [12.0.1.1.0] - 2024-11-09
### Added
- [#88](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/88) Fine tunning

## [12.0.1.0.0] - 2024-10-09
### Added
- [#84](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/84) Add helpdesk_ticket_mail_message module
