[![License: AGPL-3](https://img.shields.io/badge/licence-AGPL--3-blue.png)](http://www.gnu.org/licenses/agpl-3.0-standalone.html)
[![Alpha](https://img.shields.io/badge/maturity-Mature-brightgreen.png)](https://odoo-community.org/page/development-status)


This project aims to build odoo addons that improves helpdesk ticket management and integration with external servers.


## Addons list

addon | version | summary
---|---|---
odoo12-addon-helpdesk-automove | 12.0.0.2.0 | Allows to define automatic stage movements for helpdesk tickets.
odoo12-addon-helpdesk-ticket-api | 12.0.0.1.0 | Creates a helpdesk ticket API.
odoo12-addon-helpdesk-ticket-contract-contract | 12.0.2.0.0.99.dev1 | Allows to link helpdesk tickets with contracts.
odoo12-addon-helpdesk-ticket-document-pages | 12.0.2.0.0.99.dev1 | Allows to relate helpdesk tickets with knowledge document pages.
odoo12-addon-helpdesk-ticket-massive-creation | 12.0.2.0.0.99.dev1 | Create helpdesk tickets massively, from multiple preselected partners or contracts.


## Development

### Configure local development environment

#### Python environment

First of all, to start to development, we need to create a virtualenv with python 3.8 in our local machine.

In your local environment, where you execute the `git commit ...` command, run:

1. Install `pyenv`
```sh
curl https://pyenv.run | bash
```
2. Build the Python version
```sh
pyenv install  3.8.13
```
3. Create a virtualenv
```sh
pyenv virtualenv 3.8.13 odoo-helpdesk
```
4. Activate the virtualenv
```sh
pyenv activate odoo-helpdesk
```
5. Install dependencies
```sh
pip install pre-commit
```
5. Install pre-commit hooks
```sh
pre-commit install
```

#### LXC container and mounting

Reuse a local odoo lxc container, or create a new one. Follow the [instructions](https://gitlab.com/coopdevs/odoo-somconnexio-inventory#requirements) in [odoo-somconnexio-inventory](https://gitlab.com/coopdevs/odoo-somconnexio-inventory).

To check our local lxc containers and their status, run:
```sh
$ sudo lxc-ls -f
```

Then, mount the development folder (containing all developed modules) into the local container under the following path:

```sh
lxc.mount.entry = /home/<user>/<path>/odoo-helpdesk /var/lib/lxc/<odoo-container>/rootfs/opt/odoo_modules/odoo-helpdesk none bind,create=dir 0.0
```

And make sure this path is included in the `addons_path` from the odoo conf file ("/etc/odoo/odoo.conf"):

```
    ; Custom Modules
    addons_path = /opt/odoo/addons, /opt/odoo_modules, /opt/odoo_modules/odoo-helpdesk
```

Once created, we can stop or start our lxc container as indicated here:
```sh
$ sudo systemctl start lxc@<odoo-container>
$ sudo systemctl stop lxc@<odoo-container>
```

#### Start the ODOO application

Enter to your local machine as the user `odoo`, activate the python enviornment first and run the odoo bin:
```sh
$ ssh odoo@<odoo-container>.local
$ pyenv activate odoo
$ cd /opt/odoo
$ set -a && source /etc/default/odoo && set +a
$ ./odoo-bin -c /etc/odoo/odoo.conf -u <<odoo-module>> -d odoo --workers 0
```
