# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.0.1.1] - 2024-10-01
- [#80](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/34) Fix demo data

## [12.0.0.1.0] - 2024-06-06
- [#34](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/34) Add mail_activity_mail_template module
