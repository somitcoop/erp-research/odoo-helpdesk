#####################
 Helpdesk Ticket API
#####################

..
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !! This file is generated by oca-gen-addon-readme !!
   !! changes will be overwritten.                   !!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !! source digest: sha256:fa88a5911e9d75e4ccaee78931e61a91ccfe34e0de09254e0edd30ad0752f428
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

.. |badge1| image:: https://img.shields.io/badge/maturity-Beta-yellow.png
   :alt: Beta
   :target: https://odoo-community.org/page/development-status

.. |badge2| image:: https://img.shields.io/badge/licence-AGPL--3-blue.png
   :alt: License: AGPL-3
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html

|badge1| |badge2|

This module exposes an API-Key authenticated API to get and create
helpdesk tickets

**Table of contents**

.. contents::
   :local:

***************
 Configuration
***************

To give access to the API to a structure, go to

-  Settings > Technical (debug mode) > Auth API Key
-  Click create and select a user, save.
-  Communicate the API-KEY to those in need to use the API.

*******
 Usage
*******

Create tiquets
==============

Example of curl to create a ticket with two variables to fill: -
`API_KEY`: authorized key from the odoo server's API-KEY (see previows
paragraph) - `ODOO_URL`: target ODOO server's URL - `CONTENT`: base64
encoded image to attach to the ticket

Tickets can be created by just one of the following parameters: -
`partner_ref` - `partner_email` - `partner_vat` - `contract_code`

-  "attachments" is an array of objects, optional, each one with the
   following fields: - `filename`: name of the file - `content`: base64
   encoded file - `mimetype`: mimetype of the file

.. code:: bash

   CONTENT=$(base64 image.jpg)

   curl -X POST \
     -H "accept: application/json" \
     -H "api-key: $APIKEY" \
     -H "Content-Type: application/json" \
     -d "$(jq -n --arg content "$CONTENT" \
           '{
               summary: "New Ticket",
               description: "this is an API created ticket",
               partner_ref: "2828",
               team: "HTTe_loc",
               category: "HTCa_soft",
               channel: "HTCh_web",
               priority: "0",
               tags: "HTTa_hard,HTTa_tec",
               attachments: [
                 {
                   filename: "image_name",
                   content: $content,
                   mimetype: "image/jpg"
                 }
               ]
             }')" \
     "$ODOO_URL/api/ticket"

Search tiquets
==============

Tickets can be search by, either:
   -  `partner_ref`, returning those tickets linked to the given partner
   -  `contract_code`, returning those tickets related to the given
      contract.

Also, and in any of these cases, we can filter by `stage`, using the
stage code to exclude from our result all tickets in a different stage.

.. code:: bash

   curl -X GET \
    -H  "accept: application/json" \
    -H  "api-key: $API_KEY" \
    -H  "Content-Type: application/json" \
    -d '{
      "partner_ref": "10112",
      "stage": "stage_code",
    }' \
    "$ODOO_URL/api/ticket/getlist"

************************
 Known issues / Roadmap
************************

There are no issues for the moment.

*************
 Bug Tracker
*************

Bugs are tracked on `GitLab Issues
<https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/issues>`_. In
case of trouble, please check there if your issue has already been
reported. If you spotted it first, help us smashing it by providing a
detailed and welcomed feedback.

Do not contact contributors directly about support or help with
technical issues.

*********
 Credits
*********

Authors
=======

-  SomIT SCCL
-  Som Connexio SCCL

Contributors
============

-  `SomIT SCCL <https://somit.coop>`_:

      -  Álvaro Mellado <alvaro.mellado@somit.coop>
      -  José Robles <jose.robles@somit.coop>

-  `Som Connexio SCCL <https://somconnexio.coop>`_:

      -  Gerard Funosas <gerard.funosas@somconnexio.coop>

Maintainers
===========

This module is maintained by the OCA.

.. image:: https://odoo-community.org/logo.png
   :alt: Odoo Community Association
   :target: https://odoo-community.org

OCA, or the Odoo Community Association, is a nonprofit organization
whose mission is to support the collaborative development of Odoo
features and promote its widespread use.

You are welcome to contribute. To learn how please visit
https://odoo-community.org/page/Contribute.
