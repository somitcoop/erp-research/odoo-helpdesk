# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.0.2.0] - 2024-08-30
### Added
- [#68](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/68) Improve partner search by email
- [#43](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/43) Fix README bad formatting. 

## [12.0.0.1.0] - 2024-06-18
### Added
- [#11](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/11) Add helpdesk_ticket_api module
