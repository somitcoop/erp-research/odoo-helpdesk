# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [12.0.0.3.0] - 2024-03-01
- [#21](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/21) Allow to massively create tickets also by contracts.

## [12.0.0.2.0] - 2024-02-19
- [#14](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/14) Add Readme.
- [#7](https://gitlab.com/somitcoop/erp-research/odoo-helpdesk/-/merge_requests/7) Add helpdesk ticket massive creation wizard.
