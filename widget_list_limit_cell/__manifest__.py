# Copyright 2023-SomItCoop SCCL(<https://gitlab.com/somitcoop>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "widget_list_limit_cell",
    "version": "12.0.0.1.0",
    "depends": [
        "web",
    ],
    "author": """
        Som It Cooperatiu SCCL,
        Som Connexió SCCL,
        Odoo Community Association (OCA)
    """,
    "category": "web",
    "website": "https://gitlab.com/somitcoop/erp-research/odoo-helpdesk",
    "license": "AGPL-3",
    "summary": """
        SomItCoop ODOO widget to set limit columns on list view.
    """,
    "data": [
        "views/widget_list_limit_cell_view.xml",
    ],
    "qweb": [],
    "application": False,
    "installable": True,
}
